# Hi Ubuntu-Touch on a Fairphone 4 user 

When you are a Fairphone 4 tester please file your issues here in the [FP4 issue list](https://gitlab.com/ubports/porting/community-ports/android11/fairphone-4/fairphone-fp4/-/issues) or adjust/ fintune /  comment to the existing issues in the list.

**Features & Usability**
Automated Usability (beta)

This device currently offers minimal features, most of the smart functionalities might not work yet. It can be a very nice second device.

Actors:

- [ ]  Manual brightness
- [ ]  Torchlight
- [ ]  Vibration


Camera:

- [ ]  Flashlight
- [ ]  Photo
- [ ]  Video
- [ ]  Switching between cameras


Cellular:

- [ ]  Carrier info, signal strength
- [ ]  Data connection
- [ ]  Incoming, outgoing calls
- [ ]  MMS in, out
- [ ]  PIN unlock
- [ ]  SMS in, out
- [ ]  Change audio routings
- [ ]  Voice in calls
- [ ]  Volume control in calls


Endurance:

- [ ]  24+ hours battery lifetime
- [ ]  7+ days stability


GPU:

- [ ]  Boot into UI
- [ ]  Hardware video playback


Misc:

- [ ]  AppArmor patches
- [ ]  Battery percentage
- [ ]  Offline charging
- [ ]  Online charging
- [ ]  Recovery image
- [ ]  Reset to factory defaults
- [ ]  SD card detection and access
- [ ]  RTC time
- [ ]  Shutdown / Reboot
- [ ]  Wireless External monitor
- [ ]  Waydroid


Network:

- [ ]  Bluetooth
- [ ]  Flight mode
- [ ]  Hotspot
- [ ]  NFC
- [ ]  WiFi


Sensors:

- [ ]  Automatic brightness
- [ ]  Fingerprint reader
- [ ]  GPS
- [ ]  Proximity
- [ ]  Rotation
- [ ]  Touchscreen


Sound:

- [ ]  Loudspeaker
- [ ]  Microphone
- [ ]  Volume control


USB:

- [ ]  MTP access
- [ ]  ADB access
- [ ]  Wired External monitor

